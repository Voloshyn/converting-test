
var form = (function () {
	
	var form      = $('form'),
		quotation = $('select[name="quotation"]'),
		input     = $('form input'),
		convert   = $('form button[name="convert"]'),
		result    = $('form .result'),
		from      = $('form .result .from'),
		to        = $('form .result .to'),
		iso       = $('form .result .iso'),
		error     = $('form .error');
	
	var converter = {
		do: function () {
			var ajaxURI = FURL + "?format=json";
			var sendData = {"quotation" : quotation.val(), "amount" : input.val()};
			
			$.ajax({
				url: ajaxURI,
				type: "GET",
				dataType : "json",
				data : sendData,
			})
			.done(function (data) {
				
				if (data['error']) {
					result.hide();
					error.show();
					return;
				}
				else {
					error.hide();
				}
				
				if (input.val() !== '' && data['from'] != undefined && data['to'] != undefined && data['iso'] != undefined) {
					from.html(data['from']);
					to.html(data['to']);
					iso.html(data['iso']);
					result.show();
				}
				else {
					result.hide();
				}
			});
		}
	}
	
	var main = {
		init: function () {
			result.hide()
				  .removeClass('invisible');
			
			error.hide()
				 .removeClass('invisible');
			
			input.on('input', function () {
				var value      = input.val(),
					valueFloat = parseFloat(value),
					valid      = /^\d*(\.\d*)?$/.test(value);
				
				if (valid) return;
				
				if (isNaN(valueFloat)) valueFloat = '';
				
				input.val(valueFloat);
			})
			
			convert.on('click', function () {
				converter.do();
			});
		}
	}
	
	return main;
})();

form.init();