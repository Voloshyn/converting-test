<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<title>Convert from USD</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		
		<link rel="stylesheet" href="<?=\Config::$css['library']['bootstrap']?>">
		<link rel="stylesheet" href="<?=\Config::$css['path']?>main.css">
        
		<script>
			var FURL = '<?=\Config::$url['base']?>';
		</script>
	</head>
	<body class="container">
		<header class="row">
			<h1 class="col-6 offset-3">Convert from USD</h1>
		</header>
		<main class="row">
			<form class="col-6 offset-3">
                <div class="input-group mb-3">
                    <?if (!empty($quotationList)) {?>
                        <select class="custom-select" name="quotation">
                            <?foreach ($quotationList as $key => $val) {?>
                                <option value="<?=$key?>"><?=$key?></option>
                            <?}?>
                        </select>
                    <?}?>
                    <input type="text" class="form-control" name="amount" placeholder="Enter amount" value="">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" name="convert">Convert</button>
                    </div>
                </div>
                
                <p class="result alert alert-primary margin-top-5 invisible">
                    <span class="from">100</span> USD = <span class="to">688</span> <span class="iso">CNY</span>
                </p>
                
                <p class="error alert alert-danger margin-top-5 invisible">
                    Some error has been occurred!
                </p>
			</form>
		</main>
		<script src="<?=\Config::$js['library']['jquery']?>"></script>
		<script src="<?=\Config::$js['library']['bootstrap']?>"></script>
		<script src="<?=\Config::$js['path']?>main.js"></script>
	</body>
</html>
