<?php

class Config {
	
	public static $url = array(
		'base' => '/test/'
	);
	
	public static $class = array(
		'path' => './class/'
	);
	
	public static $core = array(
		'path' => './core/'
	);
	
	public static $layout = array(
		'path'    => './template/layout/',
		'default' => [
			'index' => 'index.html.php',
		]
	);
	
	public static $js = array(
		'path' => '/test/template/js/',
		'library' => [
			'jquery' => './vendor/components/jquery/jquery.js',
			'bootstrap' => './vendor/twbs/bootstrap/dist/js/bootstrap.js',
		]
	);
	
	public static $css = array(
		'path' => '/test/template/css/',
		'library' => [
			'bootstrap' => './vendor/twbs/bootstrap/dist/css/bootstrap.css'
		]
	);
}