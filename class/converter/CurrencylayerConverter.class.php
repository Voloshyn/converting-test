<?php

namespace converter;

class CurrencylayerConverter implements ConverterInterface {
	
	
	
	private $error = false;
	private $apiUrl = "http://www.apilayer.net/api/live?access_key=778e303e1781196b2b588fd21672f314&format=1";
	private $quotationCollection;
	
	
	
	public function __construct() {
		$this->inflateQuotationCollectionFromUrl();
	}
	
	
	
	public function hasError () {
		return $this->error;
	}
	
	
	
	public function getQuotationList () {
		return $this->quotationCollection;
	}
	
	
	
	public function existQuotation ($quotation) {
		
		$quotation = trim((string) $quotation);
		
		return array_key_exists($quotation, $this->quotationCollection);
	}
	
	
	
	public function convert ($quotation, $amount) {
		
		$quotation   = trim((string) $quotation);
		$amount = (float) $amount;
		
		if ($this->error || !$this->existQuotation($quotation)) return 0;
		
		return number_format($this->quotationCollection[$quotation] * $amount, 2, '.', ' ');
	}
	
	
	
	public function getIso($quotation) {
		
		$quotation = trim((string) $quotation);
		
		if ($this->error || !$this->existQuotation($quotation)) return '';
		
		$iso = str_replace('USD', '', $quotation);
		
		if (empty($iso)) $iso = 'USD';
		
		return $iso;
	}
	
	
	private function inflateQuotationCollectionFromUrl () {
		
		try {
			
			$json = file_get_contents($this->apiUrl);
			
			$obj = json_decode($json, true);
			
			$this->quotationCollection = $obj['quotes'];
			
			if (empty($this->quotationCollection['USDCNY'])) throw new \Exception();
		}
		catch (\Exception $e) {
			$this->error = true;
		}
	}
 
}