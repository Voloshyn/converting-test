<?php

namespace converter;

interface ConverterInterface {
	
	
	public function hasError ();
	
	public function getQuotationList ();
	
	public function existQuotation ($quotation);
	
	public function convert ($quotation, $amount);
	
	public function getIso ($quotation);

}