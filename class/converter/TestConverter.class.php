<?php

namespace converter;

class TestConverter implements ConverterInterface {
	
	
	
	public function hasError () {
		return false;
	}
	
	
	
	public function getQuotationList () {
		return ['USDCNY' => 5];
	}
	
	
	
	public function existQuotation ($quotation) {
		return true;
	}
	
	
	
	public function convert ($quotation, $amount) {
		return 23.56;
	}
	
	
	public function getIso ($quotation) {
		return "CNY";
	}
}