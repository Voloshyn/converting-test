<?php

class Autoloader {
	
	public static function load ($className) {
		
		$classNameInfo = explode('\\', $className);
		
		$classFile = Config::$class['path'] . implode('/', $classNameInfo) . '.class.php';
		
		if (file_exists($classFile)) include $classFile;
	}
}

spl_autoload_register(array('Autoloader', 'load'));