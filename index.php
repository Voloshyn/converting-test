<?php

if ($_GET['format'] == 'json') {
	header('Content-Type: text/html; charset=utf-8');
}

require "vendor/autoload.php";

require 'Config.class.php';

require \Config::$core['path'] . 'Autoloader.class.php';


// $converter = new converter\TestConverter();
$converter = new converter\CurrencylayerConverter();

if ($_GET['format'] == 'json') {
	
	$result = [];
	
	if ($converter instanceof \converter\ConverterInterface) {
		$result['error'] = $converter->hasError() || !$converter->existQuotation($_GET['quotation']);
		$result['from']  = empty($_GET['amount']) ? 0 : (float) $_GET['amount'];
		$result['to']    = $converter->convert($_GET['quotation'], $result['from']);
		$result['iso']   = $converter->getIso($_GET['quotation']);
	}
	
	echo json_encode($result);
	exit;
}

$quotationList = $converter->getQuotationList();


require \Config::$layout['path'] . \Config::$layout['default']['index'];